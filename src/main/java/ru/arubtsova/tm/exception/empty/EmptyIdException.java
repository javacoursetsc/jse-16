package ru.arubtsova.tm.exception.empty;

import ru.arubtsova.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error! Empty Id...");
    }

}
