package ru.arubtsova.tm.exception.system;

import ru.arubtsova.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException(final String command) {
        super("Error! Unknown command ``"+command+"``...");
    }

}
