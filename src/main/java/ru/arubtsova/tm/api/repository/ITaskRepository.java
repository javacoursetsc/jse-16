package ru.arubtsova.tm.api.repository;

import ru.arubtsova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    List<Task> findAllByProjectId(String projectId);

    List<Task> findAll(Comparator<Task> comparator);

    void removeAllByProjectId(String projectId);

    Task bindTaskToProject(String taskId, String projectId);

    Task unbindTaskFromProject(String projectId);

    void add(Task task);

    void remove(Task task);

    void clear();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task removeOneById(String id);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

}
