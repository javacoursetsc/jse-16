package ru.arubtsova.tm.api.controller;

public interface ITaskController {

    void showList();

    void create();

    void clear();

    void showTaskByIndex();

    void showTaskById();

    void showTaskByName();

    void removeTaskByIndex();

    void removeTaskById();

    void removeTaskByName();

    void updateTaskByIndex();

    void updateTaskById();

    void startTaskByIndex();

    void startTaskById();

    void startTaskByName();

    void finishTaskByIndex();

    void finishTaskById();

    void finishTaskByName();

    void changeTaskStatusByIndex();

    void changeTaskStatusById();

    void changeTaskStatusByName();

}
