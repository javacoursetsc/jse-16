package ru.arubtsova.tm.api.service;

import ru.arubtsova.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
